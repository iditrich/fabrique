## clients

/v1/clients [POST] -> None /v1/clients/{id} [PUT, DELETE] -> None
/v1/clients/{client_id}/subscribe/{distribution_id} [PUT] -> None
/v1/clients/{client_id}/unsubscribe/{distribution_id} [PUT] -> None

## distributions

/v1/distributions [GET] -> List[DistributionsResponseV1]
/v1/distributions/{id} [GET] -> DistributionsResponseV1
/v1/distributions/{id} [PUT, DELETE] -> None
/v1/distributions [POST] -> None
