CREATE SEQUENCE client_id_seq START 1;
CREATE SEQUENCE distribution_id_seq START 1;
CREATE SEQUENCE subscribe_id_seq START 1;
CREATE SEQUENCE message_id_seq START 1;

CREATE TABLE client
(
    id            bigint DEFAULT nextval('client_id_seq'),
    tel_number    character varying(255) NOT NULL,
    tag           character varying(255),
    timezone      character varying(255),
    operator_code integer,
    PRIMARY KEY (id)
);

CREATE TABLE distribution
(
    id              bigint DEFAULT nextval('distribution_id_seq'),
    timestamp_begin timestamp,
    message         character varying(255),
    timestamp_end   timestamp,
    filter          json,
    PRIMARY KEY (id)
);

CREATE TABLE subscribe
(
    client_id       bigint NOT NULL,
    distribution_id bigint NOT NULL,
    PRIMARY KEY (client_id, distribution_id),
    FOREIGN KEY (client_id) REFERENCES client (id),
    FOREIGN KEY (distribution_id) REFERENCES distribution (id)
);

CREATE TABLE message
(
    id              bigint           DEFAULT nextval('message_id_seq'),
    timestamp_send  timestamp,
    status          boolean NOT NULL DEFAULT false,
    distribution_id bigint  NOT NULL,
    client_id       bigint  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE,
    FOREIGN KEY (distribution_id) REFERENCES distribution (id) ON DELETE CASCADE
);