import sqlalchemy as sa

metadata = sa.MetaData()

clients = sa.Table(
    'client',
    metadata,
    sa.Column('id', sa.BigInteger, primary_key=True, server_default=sa.Sequence('client_id_seq').next_value()),
    sa.Column('tel_number', sa.String, nullable=False),
    sa.Column('tag', sa.String),
    sa.Column('timezone', sa.String),
    sa.Column('operator_code', sa.Integer),
)

messages = sa.Table(
    'message',
    metadata,
    sa.Column('id', sa.BigInteger, primary_key=True, server_default=sa.Sequence('message_id_seq').next_value()),
    sa.Column('timestamp_send', sa.TIMESTAMP, nullable=False),
    sa.Column('status', sa.Boolean, nullable=False, server_default='false'),
    sa.Column('distribution_id', sa.BigInteger, nullable=False),
    sa.Column('client_id', sa.BigInteger, nullable=False),
)

distributions = sa.Table(
    'distribution',
    metadata,
    sa.Column('id', sa.BigInteger, primary_key=True, server_default=sa.Sequence('distribution_id_seq').next_value()),
    sa.Column('timestamp_begin', sa.TIMESTAMP, nullable=False),
    sa.Column('message', sa.String, nullable=False),
    sa.Column('timestamp_end', sa.TIMESTAMP, nullable=False),
    sa.Column('filter', sa.JSON, nullable=False),
)

subscribes = sa.Table(
    'subscribe',
    metadata,
    sa.Column('client_id', sa.BigInteger, nullable=False, primary_key=True),
    sa.Column('distribution_id', sa.BigInteger, nullable=False, primary_key=True)
)
