import asyncio
import aiohttp
import logging
from src.sender.settings import timeout_sec
from typing import List
from datetime import datetime, timedelta
from sqlalchemy.future import Engine
from src.messages.service import MessageService
from src.distributions.service import DistributionService
from src.distributions.models import DistributionFilter, DistributionsResponseV1, DistributionAddUpdateRequestV1
from src.clients.service import ClientService
from src.clients.models import ClientResponseV1
from src.subscribes.service import SubscribeService
from src.subscribes.models import SubscribeResponseV1
from src.messages.models import MessageAddRequestV1, MessageResponseV1, MessageUpdateRequestV1

logger = logging.getLogger(__name__)

jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODE4MTA0ODIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlBldGVyIn0.HKpy2h-kt9q9Znf7pky0kgdul-Wr7PfkMm7BTNACG6o'
headers = {'Authorization': f'Bearer {jwt_token}'}
timeout = aiohttp.ClientTimeout(total=timeout_sec)

class Sender:
    task = None
    queue = []

    def __init__(self, engine: Engine) -> None:
        self.distribution_service = DistributionService(engine)
        self.message_service = MessageService(engine)
        self.client_service = ClientService(engine)
        self.subscribe_service = SubscribeService(engine)

    async def send(self, client: ClientResponseV1, distribution: DistributionsResponseV1) -> None:
        message_id = await self.message_service.add_message(
            MessageAddRequestV1(
                timestamp_send=datetime.now(),
                client_id=client.id,
                distribution_id=distribution.id)
        )
        json_data = {"id": int(message_id), "phone": client.tel_number, "text": distribution.message}
        async with aiohttp.ClientSession(timeout=timeout) as session:
            async with session.post(f'https://probe.fbrq.cloud/v1/send/{message_id}', headers=headers, ssl=False,
                                    json=json_data) as response:

                if response.status == 200:
                    await self.message_service.update_message(MessageUpdateRequestV1(
                        id=message_id,
                        timestamp_send=datetime.now(),
                        status=True
                    ), message_id)
                    logger.warning(f'message {message_id} sent')
                else:
                    logger.warning(f'message {message_id} not sent, code {response.status}')

    async def resend(self, message: MessageResponseV1, distribution: DistributionsResponseV1) -> None:
        client = await self.client_service.get_client_by_id(message.client_id)
        json_data = {"id": int(message.id), "phone": client.tel_number, "text": distribution.message}
        async with aiohttp.ClientSession(timeout=timeout) as session:
            async with session.post(f'https://probe.fbrq.cloud/v1/send/{message.id}',
                                    headers=headers,
                                    ssl=False,
                                    json=json_data) as response:

                if response.status == 200:
                    await self.message_service.update_message(MessageUpdateRequestV1(
                        timestamp_send=datetime.now(),
                        status=True
                    ), message.id)
                    logger.warning(f'message {message.id} resent')
                else:
                    logger.warning(f'message {message.id} not resent, code {response.status}')

    async def check_client(self, client: ClientResponseV1, filter: DistributionFilter,
                           subscribes: List[SubscribeResponseV1], messages: List[MessageResponseV1]) -> bool:
        for message in messages:
            if message.client_id == client.id:  # if there is message associated with client
                return False

        if client.tag == filter.tag:  # filter by tag
            if filter.operator_code is not None:
                if client.operator_code == filter.operator_code:
                    return True
                return False
            return True

        if client.operator_code == filter.operator_code:  # filter by operator_code
            if filter.tag is not None:
                if client.tag == filter.tag:
                    return True
                return False
            return True

        for subscribe in subscribes:  # filter by subscribes
            if subscribe.client_id == client.id:
                return True
        return False

    async def task_func(self) -> None:
        while True:

            distributions_data = await self.distribution_service.get_all_distributions()
            sleep_time = timedelta(-1)

            for distribution_data in distributions_data:
                if distribution_data.timestamp_end > datetime.now():  # filter overdue distributions
                    if distribution_data.timestamp_begin <= datetime.now():  # if distribution begin time already began

                        subscribes = await self.subscribe_service.get_subscribes_by_distribution_id(distribution_data.id)
                        messages = await self.message_service.get_messages_by_distribution_id(distribution_data.id)
                        clients = await self.client_service.get_all_clients()
                        for client in clients:
                            check = await self.check_client(client, distribution_data.filter, subscribes, messages)
                            if check:
                                await self.send(client, distribution_data)

                        for message in messages:
                            if not message.status:
                                await self.resend(message, distribution_data)
                    if distribution_data.timestamp_begin > datetime.now():  # if distribution begin time did not began
                        new_sleep_time = distribution_data.timestamp_begin - datetime.now()

                        if sleep_time > timedelta(0):
                            if new_sleep_time < sleep_time:
                                sleep_time = new_sleep_time
                        else:
                            sleep_time = new_sleep_time

            if sleep_time > timedelta(0):
                await asyncio.sleep(sleep_time.seconds)
            else:
                await self.stop_task()

    async def start_task(self) -> None:
        self.task = asyncio.ensure_future(self.task_func())

    async def restart_task(self) -> None:
        print('restarted')
        self.task.cancel()
        await self.start_task()

    async def stop_task(self) -> None:
        self.task.cancel()
