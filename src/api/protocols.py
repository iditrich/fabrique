from typing import List
from pydantic import Field
from src.subscribes.models import SubscribeAddRequestV1
from src.clients.models import ClientAddUpdateRequestV1, ClientResponseV1
from src.messages.models import MessageResponseV1
from src.subscribes.models import SubscribeResponseV1
from src.distributions.models import DistributionsResponseV1, DistributionAddUpdateRequestV1, DistributionFilter


class SubscribeServiceProtocol:
    async def __prepare(self, subscribe: SubscribeAddRequestV1) -> dict:
        raise NotImplementedError

    async def add_subscribe(self, client_id: int = Field(..., ge=1), distribution_id: int = Field(..., ge=1)) -> None:
        raise NotImplementedError

    async def delete_subscribe_by_id(self, id: int = Field(..., ge=1)) -> None:
        raise NotImplementedError

    async def delete_subscribe(self, client_id: int, distribution_id: int) -> None:
        raise NotImplementedError


class ClientServiceProtocol:
    async def __prepare(self, subscribe: ClientAddUpdateRequestV1) -> dict:
        raise NotImplementedError

    async def add_client(self, client: ClientAddUpdateRequestV1) -> None:
        raise NotImplementedError

    async def update_client(self, client: ClientAddUpdateRequestV1, id: int) -> None:
        raise NotImplementedError

    async def delete_client_by_id(self, id: int) -> None:
        raise NotImplementedError

    async def get_client_by_id(self, id: int) -> ClientResponseV1:
        raise NotImplementedError


class DistributionServiceProtocol:
    async def __prepare(self, subscribe: DistributionAddUpdateRequestV1) -> dict:
        raise NotImplementedError

    async def add_distribution(self, distribution: DistributionAddUpdateRequestV1) -> None:
        raise NotImplementedError

    async def get_all_distributions(self) -> List[DistributionsResponseV1]:
        raise NotImplementedError

    async def get_distribution_by_id(self, id: int) -> DistributionsResponseV1:
        raise NotImplementedError

    async def update_distribution(self, client: DistributionAddUpdateRequestV1, id: int) -> None:
        raise NotImplementedError

    async def delete_distribution_by_id(self, id: int) -> None:
        raise NotImplementedError

    async def get_all_clients(self) -> List[ClientResponseV1]:
        raise NotImplementedError


class SenderProtocol:
    async def send(self, client: ClientResponseV1, distribution: DistributionsResponseV1) -> None:
        raise NotImplementedError

    async def resend(self, message: MessageResponseV1, distribution: DistributionsResponseV1) -> None:
        raise NotImplementedError

    async def task_func(self) -> None:
        raise NotImplementedError

    async def start_task(self) -> None:
        raise NotImplementedError

    async def check_client(self, client: ClientResponseV1, filter: DistributionFilter,
                           subscribes: List[SubscribeResponseV1], messages: List[MessageResponseV1]) -> bool:
        raise NotImplementedError

    async def restart_task(self) -> None:
        raise NotImplementedError

    async def stop_task(self) -> None:
        raise NotImplementedError
