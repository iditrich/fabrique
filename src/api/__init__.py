import asyncio

from fastapi import FastAPI
from src.api import clients, protocols, distributions
from src.clients.service import ClientService
from src.distributions.service import DistributionService
from src.subscribes.service import SubscribeService
from src.database import DatabaseSettings, create_database_url
from sqlalchemy.ext.asyncio import create_async_engine
from src.sender import Sender

db_settings = DatabaseSettings()

engine = create_async_engine(
    create_database_url(db_settings),
    future=True
)
sender = Sender(engine)


def get_application() -> FastAPI:
    application = FastAPI(
        title='Message service',
        description='Сервис уведомлений.',
        version='1.0.0'
    )
    application.include_router(clients.router)
    application.include_router(distributions.router)

    client_service = ClientService(engine)
    distribution_service = DistributionService(engine)
    subscribe_service = SubscribeService(engine)

    application.dependency_overrides[protocols.SenderProtocol] = lambda: sender
    application.dependency_overrides[protocols.ClientServiceProtocol] = lambda: client_service
    application.dependency_overrides[protocols.DistributionServiceProtocol] = lambda: distribution_service
    application.dependency_overrides[protocols.SubscribeServiceProtocol] = lambda: subscribe_service

    return application


asyncio.create_task(sender.start_task())

app = get_application()
