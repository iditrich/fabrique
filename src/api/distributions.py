import logging
from typing import List
from fastapi import APIRouter, status, Depends, Path
from src.api.protocols import DistributionServiceProtocol, SenderProtocol
from src.distributions.models import DistributionAddUpdateRequestV1, DistributionsResponseV1, DistributionResponseV1

logger = logging.getLogger(__name__)

router = APIRouter(
    tags=['distributions']
)


@router.get(
    path='/v1/distributions',
    response_model=List[DistributionsResponseV1],
    summary='Статистика по рассылкам',
    description='Возвращает статистику по всем рассылкам.',
)
async def get_all_distributions(
        distribution_service: DistributionServiceProtocol = Depends(),
) -> List[DistributionsResponseV1]:
    logger.warning(f'[GET] /v1/distributions')
    return await distribution_service.get_all_distributions()


@router.get(
    path='/v1/distributions/{id}',
    response_model=DistributionResponseV1,
    summary='Статистика по рассылке',
    description='Возвращает статистику по одной рассылке.',
)
async def get_distribution_by_id(
        distribution_service: DistributionServiceProtocol = Depends(),
        id: int = Path(..., ge=1),
) -> DistributionsResponseV1:
    logger.warning(f'[GET] /v1/distributions/{id}')
    return await distribution_service.get_distribution_by_id(id)


@router.put(
    path='/v1/distributions/{id}',
    status_code=status.HTTP_200_OK,
    summary='Обновить информацию о рассылке',
    description='Обновляет информацию о рассылке.',
)
async def update_distribution(
        distribution_data: DistributionAddUpdateRequestV1,
        distribution_service: DistributionServiceProtocol = Depends(),
        id: int = Path(..., ge=1),
        sender: SenderProtocol = Depends()
):
    logger.warning(f'[PUT] /v1/distributions/{id} {distribution_data.filter, distribution_data.message, distribution_data.timestamp_end, distribution_data.timestamp_begin}')
    await distribution_service.update_distribution(distribution_data, id)
    await sender.restart_task()


@router.post(
    path='/v1/distributions',
    status_code=status.HTTP_201_CREATED,
    summary='Добавить рассылку',
    description='Добавляет рассылку.',
)
async def add_distribution(
        distribution_data: DistributionAddUpdateRequestV1,
        distribution_service: DistributionServiceProtocol = Depends(),
        sender: SenderProtocol = Depends()
):
    logger.warning(f'[POST] /v1/distributions {distribution_data.filter, distribution_data.message, distribution_data.timestamp_end, distribution_data.timestamp_begin}')
    await distribution_service.add_distribution(distribution_data)
    await sender.restart_task()


@router.delete(
    path='/v1/distributions/{id}',
    summary='Удалить рассылку',
    description='Удаляет рассылку.'
)
async def delete_distribution(
        id: int = Path(..., ge=1),
        distribution_service: DistributionServiceProtocol = Depends(),
        sender: SenderProtocol = Depends()
):
    logger.warning(f'[DELETE] /v1/distributions/{id}')
    await distribution_service.delete_distribution_by_id(id)
    await sender.restart_task()
