import logging
from fastapi import APIRouter, status, Depends, Path

from src.api.protocols import ClientServiceProtocol, SenderProtocol
from src.api.protocols import SubscribeServiceProtocol
from src.clients.models import ClientAddUpdateRequestV1

logger = logging.getLogger(__name__)

router = APIRouter(
    tags=['clients']
)


@router.put(
    path='/v1/clients/{id}',
    status_code=status.HTTP_200_OK,
    summary='Обновить информацию о пользователе',
    description='Обновляет информацию о пользователе.',
)
async def update_client(
        client_data: ClientAddUpdateRequestV1,
        client_service: ClientServiceProtocol = Depends(),
        id: int = Path(..., ge=1),
):
    logger.warning(f'[PUT] /v1/clients/{id} {client_data}')
    await client_service.update_client(client_data, id)


@router.post(
    path='/v1/clients',
    status_code=status.HTTP_201_CREATED,
    summary='Добавить пользователя',
    description='Добавляет пользователя для рассылок.',
)
async def add_client(
        client_data: ClientAddUpdateRequestV1,
        client_service: ClientServiceProtocol = Depends(),
        sender: SenderProtocol = Depends()
):
    logger.warning(f'[POST] /v1/clients {client_data.timezone, client_data.operator_code, client_data.tag}')
    await client_service.add_client(client_data)
    await sender.restart_task()


@router.put(
    path='/v1/clients/{client_id}/subscribe/{distribution_id}',
    status_code=status.HTTP_200_OK,
    summary='Подписать пользователя на рассылку',
    description='Подписывает пользователя на рассылку.',
)
async def subscribe_client(
        client_id: int = Path(..., ge=1),
        distribution_id: int = Path(..., ge=1),
        subscribe_client: SubscribeServiceProtocol = Depends(),
        sender: SenderProtocol = Depends()
):
    logger.warning(f'[PUT] /v1/clients/{client_id}/subscribe/{distribution_id}')
    await subscribe_client.add_subscribe(client_id, distribution_id)
    await sender.restart_task()


@router.put(
    path='/v1/clients/{client_id}/unsubscribe/{distribution_id}',
    status_code=status.HTTP_200_OK,
    summary='Отписать пользователя от рассылки',
    description='Отписывает пользователя от рассылки.',
)
async def unsubscribe_client(
        client_id: int = Path(..., ge=1),
        distribution_id: int = Path(..., ge=1),
        subscribe_client: SubscribeServiceProtocol = Depends()
):
    logger.warning(f'[PUT] /v1/clients/{client_id}/unsubscribe/{distribution_id}')
    await subscribe_client.delete_subscribe(client_id, distribution_id)


@router.delete(
    path='/v1/clients/{id}',
    summary='Удалить пользователя',
    description='Удаляет пользователя.'
)
async def delete_client(
        id: int = Path(..., ge=1),
        client_service: ClientServiceProtocol = Depends()
):
    logger.warning(f'[PUT] /v1/clients/{id}')
    await client_service.delete_client_by_id(id)
