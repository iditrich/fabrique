from pydantic import BaseModel, Field, validator
from src.messages.models import MessageResponseV1
from typing import Optional
from datetime import datetime
from typing import List


class DistributionFilter(BaseModel):
    tag: Optional[str]
    operator_code: Optional[int]


class DistributionsResponseV1(BaseModel):
    id: int = Field(..., ge=1)
    timestamp_begin: datetime
    message: str
    timestamp_end: datetime
    filter: DistributionFilter
    not_sent_messages: Optional[int]
    sent_messages: Optional[int]


class DistributionResponseV1(BaseModel):
    id: int = Field(..., ge=1)
    timestamp_begin: datetime
    message: str
    timestamp_end: datetime
    filter: DistributionFilter
    messages: Optional[List[MessageResponseV1]]


class DistributionAddUpdateRequestV1(BaseModel):
    id: int = Field(None, ge=1)
    timestamp_begin: datetime
    message: str
    timestamp_end: datetime
    filter: Optional[DistributionFilter]

    @validator('timestamp_end')
    def timestamp_end_more_than_now(cls, timestamp_end, values):
        if timestamp_end < datetime.now():
            raise ValueError('end in fast')
        if timestamp_end < values['timestamp_begin']:
            raise ValueError('end before begin')
        if timestamp_end == values['timestamp_begin']:
            raise ValueError('end and begin at the same time')
        return timestamp_end
