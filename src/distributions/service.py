from typing import List
import logging

from sqlalchemy import select, insert, delete, update
from sqlalchemy.future import Engine

from src.database import tables
from src.distributions.models import DistributionAddUpdateRequestV1, DistributionsResponseV1, DistributionResponseV1
from src.messages.models import MessageResponseV1

logger = logging.getLogger(__name__)

class DistributionService:
    def __init__(self, engine: Engine) -> None:
        self._engine = engine

    async def __prepare(self, distribution: DistributionAddUpdateRequestV1) -> dict:
        result = {}
        if distribution.id is not None:
            result['id'] = distribution.id
        if distribution.timestamp_begin is not None:
            result['timestamp_begin'] = distribution.timestamp_begin
        if distribution.message is not None:
            result['message'] = distribution.message
        if distribution.timestamp_end is not None:
            result['timestamp_end'] = distribution.timestamp_end
        if distribution.filter is not None:
            result['filter'] = distribution.filter.dict()

        return result

    async def get_all_distributions(self) -> List[DistributionsResponseV1]:
        query = select(tables.distributions)
        async with self._engine.connect() as connection:
            distributions_data = await connection.execute(query)
        distributions = []

        for distribution_data in distributions_data:
            not_sent_messages = 0
            sent_messages = 0

            messages = select(tables.messages).where(tables.messages.c.distribution_id == distribution_data['id'])
            async with self._engine.connect() as connection:
                messages_data = await connection.execute(messages)

            for message_data in messages_data:
                if message_data.status:
                    sent_messages += 1
                if not message_data.status:
                    not_sent_messages += 1

            distribution = DistributionsResponseV1(
                id=distribution_data['id'],
                timestamp_begin=distribution_data['timestamp_begin'],
                message=distribution_data['message'],
                timestamp_end=distribution_data['timestamp_end'],
                filter=distribution_data['filter'],
                not_sent_messages=not_sent_messages,
                sent_messages=sent_messages,
            )
            distributions.append(distribution)
        return distributions

    async def get_distribution_by_id(self, id: int) -> DistributionResponseV1:
        query = select(tables.distributions).where(tables.distributions.c.id == id)
        async with self._engine.connect() as connection:
            distribution_data = (await connection.execute(query)).fetchone()

        messages = select(tables.messages).where(tables.messages.c.distribution_id == distribution_data['id'])
        async with self._engine.connect() as connection:
            messages_data = await connection.execute(messages)

        messages = []
        for message_data in messages_data:
            message = MessageResponseV1(
                id=message_data['id'],
                timestamp_send=message_data['timestamp_send'],
                status=message_data['status'],
                client_id=message_data['client_id'],
                distribution_id=message_data['distribution_id'],
            )
            messages.append(message)

        distribution = DistributionResponseV1(
            id=distribution_data['id'],
            timestamp_begin=distribution_data['timestamp_begin'],
            message=distribution_data['message'],
            timestamp_end=distribution_data['timestamp_end'],
            filter=distribution_data['filter'],
            messages=messages
        )
        return distribution

    async def add_distribution(self, distribution: DistributionAddUpdateRequestV1) -> None:
        data = await self.__prepare(distribution)
        query = insert(tables.distributions).values(
            **data
        ).returning(tables.distributions.c.id)
        async with self._engine.connect() as connection:
            result = (await connection.execute(query)).fetchone()
            await connection.commit()
        logger.warning(f'added distribution {data} id:{result[0]}')

    async def delete_distribution_by_id(self, id: int) -> None:
        query = delete(tables.distributions).where(tables.distributions.c.id == id)
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()

    async def update_distribution(self, distribution: DistributionAddUpdateRequestV1, id: int) -> None:
        data = await self.__prepare(distribution)
        query = update(tables.distributions).where(tables.distributions.c.id == id).values(
            **data
        )
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()
