from typing import List
import logging

from sqlalchemy import update, insert, delete, select
from sqlalchemy.future import Engine

from src.database import tables
from src.messages.models import MessageAddRequestV1, MessageUpdateRequestV1, MessageResponseV1

logger = logging.getLogger(__name__)


class MessageService:
    def __init__(self, engine: Engine) -> None:
        self._engine = engine

    async def __prepare_add(self, message: MessageAddRequestV1) -> dict:
        result = {}
        if message.id is not None:
            result['id'] = message.id
        result['timestamp_send'] = message.timestamp_send
        if message.id is not None:
            result['status'] = message.status
        result['client_id'] = message.client_id
        result['distribution_id'] = message.distribution_id

        return result

    async def __prepare_update(self, message: MessageUpdateRequestV1) -> dict:
        result = {}
        if message.status:
            result['status'] = message.status
        if message.timestamp_send:
            result['timestamp_send'] = message.timestamp_send

        return result

    async def add_message(self, message: MessageAddRequestV1) -> int:
        data = await self.__prepare_add(message)
        query = insert(tables.messages).values(
            **data
        ).returning(tables.messages.c.id)
        async with self._engine.connect() as connection:
            result = (await connection.execute(query)).fetchone()
            await connection.commit()
        logger.warning(f'added message {data} id:{result[0]}')
        return result[0]

    async def update_message(self, message: MessageUpdateRequestV1, id: int) -> None:
        data = await self.__prepare_update(message)
        query = update(tables.messages).where(tables.messages.c.id == id).values(
            **data
        ).returning(tables.messages.c.id)
        async with self._engine.connect() as connection:
            result = (await connection.execute(query)).fetchone()
            await connection.commit()
        logger.warning(f'updated message {data} id:{result[0]}')

    async def get_messages_by_distribution_id(self, distribution_id: int) -> List[MessageResponseV1]:
        query = select(tables.messages).where(tables.messages.c.distribution_id == distribution_id)
        async with self._engine.connect() as connection:
            messages_data = await connection.execute(query)
        messages = []
        for message_data in messages_data:
            message = MessageResponseV1(
                id=message_data['id'],
                timestamp_send=message_data['timestamp_send'],
                status=message_data['status'],
                client_id=message_data['client_id'],
                distribution_id=message_data['distribution_id'],
            )
            messages.append(message)
        return messages
