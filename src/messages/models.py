from pydantic import BaseModel, Field
from datetime import datetime
from typing import Optional


class MessageResponseV1(BaseModel):
    id: int = Field(..., ge=1)
    timestamp_send: datetime
    status: Optional[bool]
    client_id: int = Field(..., ge=1)
    distribution_id: int = Field(..., ge=1)


class MessageAddRequestV1(BaseModel):
    id: int = Field(None, ge=1)
    timestamp_send: datetime
    status: Optional[bool]
    client_id: int = Field(..., ge=1)
    distribution_id: int = Field(..., ge=1)


class MessageUpdateRequestV1(BaseModel):
    timestamp_send: datetime
    status: bool
