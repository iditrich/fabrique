from pydantic import BaseModel, Field, validator
from typing import Optional
import pytz


class ClientAddUpdateRequestV1(BaseModel):
    id: int = Field(None, ge=1)
    tel_number: str
    tag: str
    timezone: Optional[str]
    operator_code: Optional[int]

    @validator('timezone')
    def check_timezone(cls, timezone):
        if timezone not in pytz.all_timezones:
            raise ValueError('incorrect timezone')
        return timezone


class ClientResponseV1(BaseModel):
    id: int = Field(..., ge=1)
    tel_number: str
    tag: str
    timezone: Optional[str]
    operator_code: Optional[int]
