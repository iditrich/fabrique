from typing import List
import logging

from sqlalchemy import update, insert, delete, select
from sqlalchemy.future import Engine

from src.database import tables
from src.clients.models import ClientAddUpdateRequestV1, ClientResponseV1

logger = logging.getLogger(__name__)

class ClientService:
    def __init__(self, engine: Engine) -> None:
        self._engine = engine

    async def __prepare(self, client: ClientAddUpdateRequestV1) -> dict:
        result = {}
        if client.id is not None:
            result['id'] = client.id
        if client.tel_number is not None:
            result['tel_number'] = client.tel_number
        if client.tag is not None:
            result['tag'] = client.tag
        if client.timezone is not None:
            result['timezone'] = client.timezone
        return result

    async def add_client(self, client: ClientAddUpdateRequestV1) -> None:
        data = await self.__prepare(client)
        query = insert(tables.clients).values(
            **data
        ).returning(tables.clients.c.id)
        async with self._engine.connect() as connection:
            result = (await connection.execute(query)).fetchone()
            await connection.commit()
        logger.warning(f'added client {data}, id:{result[0]}')

    async def delete_client_by_id(self, id: int) -> None:
        query = delete(tables.clients).where(tables.clients.c.id == id)
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()

    async def update_client(self, client: ClientAddUpdateRequestV1, id: int) -> None:
        data = await self.__prepare(client)
        query = update(tables.clients).where(tables.clients.c.id == id).values(
            **data
        )
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()

    async def get_all_clients(self) -> List[ClientResponseV1]:
        query = select(tables.clients)
        async with self._engine.connect() as connection:
            clients_data = await connection.execute(query)
        clients = []
        for client_data in clients_data:
            client = ClientResponseV1(
                id=client_data['id'],
                tel_number=client_data['tel_number'],
                tag=client_data['tag'],
                timezone=client_data['timezone'],
                operator_code=client_data['operator_code'],
            )
            clients.append(client)
        return clients

    async def get_client_by_id(self, id: int) -> ClientResponseV1:
        query = select(tables.clients).where(tables.clients.c.id == id)
        async with self._engine.connect() as connection:
            client_data = (await connection.execute(query)).fetchone()
        client = ClientResponseV1(
            id=client_data.id,
            tel_number=client_data.tel_number,
            tag=client_data.tag,
            timezone=client_data.timezone,
            operator_code=client_data.operator_code,
        )
        return client
