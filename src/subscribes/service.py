from typing import List
from pydantic import Field
from sqlalchemy import insert, delete, select
from sqlalchemy.future import Engine
from src.database import tables
from src.subscribes.models import SubscribeAddRequestV1, SubscribeResponseV1


class SubscribeService:
    def __init__(self, engine: Engine) -> None:
        self._engine = engine

    async def __prepare(self, subscribe: SubscribeAddRequestV1) -> dict:
        result = {'client_id': subscribe.client_id, 'distribution_id': subscribe.distribution_id}

        return result

    async def add_subscribe(self, client_id: int = Field(..., ge=1), distribution_id: int = Field(..., ge=1)) -> None:
        query = insert(tables.subscribes).values(
            client_id=client_id,
            distribution_id=distribution_id
        )
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()

    async def delete_subscribe(self, client_id: int = Field(..., ge=1),
                               distribution_id: int = Field(..., ge=1)) -> None:
        query = delete(tables.subscribes).where(tables.subscribes.c.client_id == client_id).where(
            tables.subscribes.c.distribution_id == distribution_id
        )
        async with self._engine.connect() as connection:
            await connection.execute(query)
            await connection.commit()

    async def get_subscribes_by_distribution_id(self, distribution_id: int = Field(..., ge=1)) -> List[
        SubscribeResponseV1]:
        query = select(tables.subscribes).where(tables.subscribes.c.distribution_id == distribution_id)
        async with self._engine.connect() as connection:
            subscribes_data = await connection.execute(query)
        subscribes = []
        for subscribe_data in subscribes_data:
            subscribe = SubscribeResponseV1(
                client_id=subscribe_data['client_id'],
                distribution_id=subscribe_data['distribution_id'],
            )
            subscribes.append(subscribe)
        return subscribes
