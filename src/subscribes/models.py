from pydantic import BaseModel, Field


class SubscribeAddRequestV1(BaseModel):
    client_id: int = Field(..., ge=1)
    distribution_id: int = Field(..., ge=1)


class SubscribeResponseV1(BaseModel):
    client_id: int = Field(..., ge=1)
    distribution_id: int = Field(..., ge=1)
