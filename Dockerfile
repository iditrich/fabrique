FROM python:3.10

ENV PYTHONPATH=/fabrique

COPY requirements.txt /

RUN apt update && \
 apt install -y libpq-dev python3-dev&& \
 pip install -r /requirements.txt && \
 rm /requirements.txt && \
 mkdir -p /fabrique/src/api

COPY src /fabrique/src

ENTRYPOINT ["/bin/bash","-c","uvicorn src.api:app --host $FABRIQUE_API_HOST --port $FABRIQUE_API_PORT --loop asyncio --log-level $FABRIQUE_LOGLEVEL"]