# fabrique

## Сборка контейнера базы

```cd database_build```
```docker build --network host -t fabrique_post .```

## Сборка контейнера api

```docker build --network host -t fabrique_api .```(из корня проекта)

## Запуск сервиса

```docker-compose up -d```(из корня проекта)(после сборки контейнеров)

## Запуск сервиса без docker

```pip3 install -r requirements.txt```(из корня проекта)

```uvicorn src.api:app --host 0.0.0.0 --port 5000 --loop asyncio```(из корня проекта)

# Выполненные дополнительные задания

+ Выполнены доп. задания номер 
  + 3 подготовить docker-compose для запуска всех сервисов проекта одной командой.
  + 5 сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. (встроенная функция fastapi) 
  + 9 удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
  + 12 обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию по:
    + id рассылки - все логи по конкретной рассылке (и запросы на api и внешние запросы на отправку конкретных сообщений)
    + id сообщения - по конкретному сообщению (все запросы и ответы от внешнего сервиса, вся обработка конкретного сообщения)
    + id клиента - любые операции, которые связаны с конкретным клиентом (добавление/редактирование/отправка сообщения/…)

+ Написан список эндпоинтов (endpoints.md)